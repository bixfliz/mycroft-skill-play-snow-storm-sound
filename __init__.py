import random
from os.path import dirname, join

from adapt.intent import IntentBuilder
from mycroft import MycroftSkill, intent_handler
from mycroft.skills.audioservice import AudioService
from mycroft.audio import wait_while_speaking

class SnowStormSoundSkill(MycroftSkill):
    def __init__(self):
        super(SnowStormSoundSkill, self).__init__(name="SnowStormSoundSkill")
        self.process = None

    def initialize(self):
        self.audioservice = AudioService(self.bus)
        self.add_event("mycroft.play_snow_storm_sound", self.play_snow_storm_sound, False)

    def play_snow_storm_sound(self, message):
        self.process = play_mp3(join(dirname(__file__), "wind.mp3"))

    @intent_handler(IntentBuilder('').require('SnowStormSound'))
    def handle_play_snow_storm_sound(self, message):
        try:
            self.speak_dialog('playing.snow.storm.sound')
            wait_while_speaking()
            self.audioservice.play(join(dirname(__file__), "wind.mp3"))
        except Exception as e:
            self.log.error("Error: {0}".format(e))

    def stop(self):
        if self.process and self.process.poll() is None:
            self.speak_dialog('playing.snow.storm.sound.stop')
            self.process.terminate()
            self.process.wait()


def create_skill():
    return SnowStormSoundSkill()
